import { Component } from "@angular/core";

@Component({
    selector:'menu-list',
    templateUrl:'./menu.html'
})
export class Menu{
    links = [
        {path:'/',label:'Home', active:'false'},
        {path:'/mangos',label:'Mangos', active:'false'},
        {path:'/mangos/add',label:'Add Mango', active:'false'}
        
    ]
}