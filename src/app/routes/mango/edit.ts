import {Component, OnInit} from "@angular/core";
import { MangosModel } from "../../services/mangos.model";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
    templateUrl:'./edit.html'
})
export class MangoEdit implements OnInit{
    
    id;
    mango = {};
    colors = [
        {label:"Red", value:'red'},
        {label:"Orange", value:'orange'},
        {label:"Green", value:'green'},
        {label:"Yellow", value:'yellow'},
        {label:"Lime", value:'lime'},
    ];

    constructor(private mangosModel:MangosModel, private route:ActivatedRoute, private router: Router){
        
    }

    ngOnInit(){
        this.route.parent.params.subscribe((params)=>this.id = params.id);
        this.mangosModel.getMango(this.id);       
    }

    editMango(){
        this.mangosModel.editMango(this.mangosModel.mango, ()=>{
            this.router.navigate(['/mangos']);
        });
    }

}