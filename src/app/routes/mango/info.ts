import {Component, OnInit} from "@angular/core";
import { MangosModel } from "../../services/mangos.model";
import { ActivatedRoute } from "@angular/router";
import 'rxjs/add/operator/map';
@Component({
    templateUrl:'./info.html'
})
export class MangoInfo implements OnInit{
    
    id;

    constructor(private mangosModel:MangosModel, private route:ActivatedRoute){
        
    }

    ngOnInit(){
        this.route.parent.params.subscribe((params)=>this.id = params.id);
        this.mangosModel.getMango(this.id);   
    }

}