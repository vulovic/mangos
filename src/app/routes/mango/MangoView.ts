import {Component} from "@angular/core";
import { ActivatedRoute } from "@angular/router";

@Component({
    templateUrl:'./mango.html'
})
export class MangoView {
    
    id;

    constructor(private route: ActivatedRoute){
        this.route.params.subscribe((params)=>this.id = params.id);
    }
    
}