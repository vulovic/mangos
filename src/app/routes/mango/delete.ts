import {Component, OnInit} from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { MangosModel } from "../../services/mangos.model";

@Component({
    templateUrl:'./delete.html'
})
export class MangoDelete implements OnInit{
    
    id;

    constructor(private route:ActivatedRoute, private mangosModel:MangosModel, private router:Router){}

    deleteMango(){
        this.mangosModel.deleteMango(this.id, ()=>{
            this.router.navigate(['/mangos']);
        });
    }

    ngOnInit(){
        this.route.parent.params.subscribe((params) => {
            this.id = params.id;
        });
    }

}