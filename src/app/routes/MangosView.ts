import {Component} from "@angular/core";
import { MangosModel } from "../services/mangos.model";
import { Router } from "@angular/router";

@Component({
    templateUrl:'./mangos.html'
})
export class MangosView {

    filterInput = {};
    searchKey = '';

    tableConfig = [
        { key: 'id', label: 'ID', type: 'text', typeof:'number' },
        { key: 'nutritionValue', label: 'Nutrition Value', type: 'text', typeof:'number' },
        { key: 'weight', label: 'Weight', type: 'text', typeof:'text'  },
        { key: 'color', label: 'Color', type: 'color', typeof:'text' }
    ];

    focus(key){
      this.searchKey = key;
    }

    constructor(private mangosModel:MangosModel, private router: Router){}

    viewMangoData(id){
        this.router.navigate(['/mangos',id]);
    }

    editMangoData(id){
        this.router.navigate(['/mangos',id,'edit']);
    }

    deleteMango(id){
        this.router.navigate(['/mangos',id,'delete']);
    }
}
