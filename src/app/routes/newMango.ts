import {Component} from "@angular/core";
import { MangosModel } from "../services/mangos.model";
import {Router} from "@angular/router";

@Component({
    templateUrl:'./newMango.html'
})
export class NewMango {

    constructor(private mangoModel:MangosModel, private router:Router){}
    
    colors = [
        {label:"Red", value:'red'},
        {label:"Orange", value:'orange'},
        {label:"Green", value:'green'},
        {label:"Yellow", value:'yellow'}
    ];

    mango = {nutritionValue:0, color:"", weight:""};

    addMango(){
        var {nutritionValue, color, weight} = this.mango;
        if(nutritionValue >= 0 && weight.length !== 0){
            this.mangoModel.addMango(this.mango, ()=>{
                this.router.navigate(['/mangos']);
            });
        } else console.log("Weight and nutrition value are required");
    }
}