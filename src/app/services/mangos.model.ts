import { Injectable } from "@angular/core";
import { Mango } from '../interfaces/mango';
import { MangosService } from "./mangos.service";

@Injectable()
export class MangosModel{
    
    mangos = [];
    mango = {};

    constructor(private mangosService: MangosService){
        this.initMangos();
    }

    initMangos(){
        this.mangosService.getMangosObservable().subscribe(mangos => this.mangos = mangos);        
    }

    deleteMango(id, clbk){
        this.mangosService.deleteMangoObservable(id).subscribe(() => {
            this.initMangos();
            clbk();
        });
    }

    addMango(data, clbk){
        this.mangosService.addMangoObservable(data).subscribe(mangos => {
            this.mangos.push(mangos);
            clbk();
        });
    }

    getMango(id){
        this.mangosService.getMangoObservable(id).subscribe(mango => this.mango = mango);
    }

    editMango(data, clbk){
        this.mangosService.updateMangoObservable(data).subscribe(mango=>{
            var i = 0;
             for(let current of this.mangos){
                  this.mangos[i] = parseInt(current.id) === parseInt(mango.id) ? mango : current;
                  i++;
             }
             clbk();
        });
    }


}