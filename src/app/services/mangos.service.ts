import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import "rxjs/add/operator/map";

@Injectable()
export class MangosService{
    
    constructor(private http:Http){}

    getMangosObservable(){
        return this.http.get('http://localhost:3000/mangos').map((res) => res.json());
    }

    getMangoObservable(id){
        return this.http.get('http://localhost:3000/mangos/'+id).map((res) => res.json());
    }

    addMangoObservable(data){
        return this.http.post('http://localhost:3000/mangos', data).map((res) => res.json());
    }

    updateMangoObservable(data){
        return this.http.put('http://localhost:3000/mangos/'+data.id, data).map((res) => res.json());
    }

    deleteMangoObservable(id){
        return this.http.delete('http://localhost:3000/mangos/'+id).map((res) => res.json());
    }

}