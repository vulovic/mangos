import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(values: any, searchQuery:any, searchKey:any): any {
    return searchQuery ? values.filter((mango) => String(mango[searchKey]).toLowerCase().indexOf(searchQuery.toLowerCase()) > -1 ) : values;
  }

}
