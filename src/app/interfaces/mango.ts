export interface Mango{
    id: number,
    nutritionValue: number,
    weight: string,
    color?: string
}