import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';

import { AppComponent } from './app.component';
import { HomeView } from './routes/HomeView';
import { MangosView } from './routes/MangosView';
import { Menu } from './components/menu';
import { MangoView } from './routes/mango/MangoView';
import { MangoInfo } from './routes/mango/info';
import { MangoEdit } from './routes/mango/edit';
import { MangosService } from './services/mangos.service';
import { MangosModel } from './services/mangos.model';
import { HttpModule } from '@angular/http';
import { MangoDelete } from './routes/mango/delete';
import { NewMango } from './routes/newMango';
import { SearchPipe } from './pipes/search.pipe';

const routes:Routes = [
  {path:'', component: HomeView},
  {path:'mangos', component: MangosView},
  {path:'mangos/add', component: NewMango}, 
  {path:'mangos/:id', component: MangoView, children: [
    {path: '', redirectTo: 'info', pathMatch:'full'},
    {path: 'edit', component: MangoEdit},
    {path: 'info', component: MangoInfo},
    {path: 'delete', component: MangoDelete}
  ]},
  {path:'**', redirectTo:'/'}
];
@NgModule({
  declarations: [
    AppComponent,
    HomeView,
    MangosView,
    MangoView,
    Menu,
    MangoInfo,
    MangoEdit,
    MangoDelete,
    NewMango,
    SearchPipe
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    HttpModule,
    FormsModule
  ],
  providers: [MangosService, MangosModel],
  bootstrap: [AppComponent]
})
export class AppModule { }
